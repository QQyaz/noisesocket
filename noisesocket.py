import socket
from enum import IntEnum
from typing import Union, List

from noise.connection import NoiseConnection, Keypair
from noise.backends.experimental import noise_backend

class NSCurve(IntEnum):
  def prefix(): return "NOISE_DH_"
  
  NOISE_DH_25519 = 1

class NSCipher(IntEnum):
  def prefix(): return "NOISE_CIPHER_"
  
  NOISE_CIPHER_ChaChaPoly = 1
  NOISE_CIPHER_AESGCM     = 2
  NOISE_CIPHER_AESCCM     = 9

class NSHash(IntEnum):
  def prefix(): return "NOISE_HASH_"
  
  NOISE_HASH_BLAKE2s = 1
  NOISE_HASH_BLAKE2b = 2
  NOISE_HASH_SHA256  = 3
  NOISE_HASH_SHA512  = 4

class NSPattern(IntEnum):
  def prefix(): return "NOISE_PATTERN_"
  
  NOISE_PATTERN_NN = 1
  NOISE_PATTERN_NK = 2
  NOISE_PATTERN_NX = 3
  NOISE_PATTERN_KN = 4
  NOISE_PATTERN_KK = 5
  NOISE_PATTERN_KX = 6
  NOISE_PATTERN_XN = 7
  NOISE_PATTERN_XK = 8
  NOISE_PATTERN_XX = 9
  
  NOISE_PATTERN_N  = 10
  NOISE_PATTERN_K  = 11
  NOISE_PATTERN_X  = 12
  
  NOISE_PATTERN_IN = 13
  NOISE_PATTERN_IK = 14
  NOISE_PATTERN_IX = 15
  
  NOISE_PATTERN_NNpsk0 = 16
  NOISE_PATTERN_NKpsk0 = 17
  NOISE_PATTERN_NXpsk0 = 18
  NOISE_PATTERN_KNpsk0 = 19
  NOISE_PATTERN_KKpsk0 = 20
  NOISE_PATTERN_KXpsk0 = 21
  NOISE_PATTERN_XNpsk0 = 22
  NOISE_PATTERN_XKpsk0 = 23
  NOISE_PATTERN_XXpsk0 = 24
  NOISE_PATTERN_Npsk0  = 25
  NOISE_PATTERN_Kpsk0  = 26
  NOISE_PATTERN_Xpsk0  = 27
  NOISE_PATTERN_INpsk0 = 28
  NOISE_PATTERN_IKpsk0 = 29
  NOISE_PATTERN_IXpsk0 = 30
  
  NOISE_PATTERN_NNpsk1 = 31
  NOISE_PATTERN_NKpsk1 = 32
  NOISE_PATTERN_NXpsk1 = 33
  NOISE_PATTERN_KNpsk1 = 34
  NOISE_PATTERN_KKpsk1 = 35
  NOISE_PATTERN_KXpsk1 = 36
  NOISE_PATTERN_XNpsk1 = 37
  NOISE_PATTERN_XKpsk1 = 38
  NOISE_PATTERN_XXpsk1 = 39
  NOISE_PATTERN_Npsk1  = 40
  NOISE_PATTERN_Kpsk1  = 41
  NOISE_PATTERN_Xpsk1  = 42
  NOISE_PATTERN_INpsk1 = 43
  NOISE_PATTERN_IKpsk1 = 44
  NOISE_PATTERN_IXpsk1 = 45
  
  NOISE_PATTERN_NNpsk2 = 46
  NOISE_PATTERN_NKpsk2 = 47
  NOISE_PATTERN_NXpsk2 = 48
  NOISE_PATTERN_KNpsk2 = 49
  NOISE_PATTERN_KKpsk2 = 50
  NOISE_PATTERN_KXpsk2 = 51
  NOISE_PATTERN_XNpsk2 = 52
  NOISE_PATTERN_XKpsk2 = 53
  NOISE_PATTERN_XXpsk2 = 54
  NOISE_PATTERN_INpsk2 = 55
  NOISE_PATTERN_IKpsk2 = 56
  NOISE_PATTERN_IXpsk2 = 57
  
  NOISE_PATTERN_XNpsk3 = 58
  NOISE_PATTERN_XKpsk3 = 59
  NOISE_PATTERN_XXpsk3 = 60

class NSNegotiationType(IntEnum):
  NOISE_SOCKET_PROTOCOL_PROPOSAL        = 0 # good
  NOISE_SOCKET_REJECTION                = 1 # error
  NOISE_SOCKET_ACCEPTANCE               = 2 # good
  NOISE_SOCKET_REINITIALIZATION_REQUEST = 3 # good

# TODO: Add supported patterns and primitives
# TODO: Add negotiation parsing 

class NoiseSocketError(Exception):
    pass

def byte (enum_el: IntEnum):
  return int(enum_el).to_bytes(1, byteorder='big')

class NoiseSocketConnection(object):
    version = bytes([0x00, 0x01])
    empty_data = bytes([0x00, 0x00])
    
    def __init__(self, sock):
        self.noise = None
        self.socket = sock

    def make_from_name(self, name: Union[str, bytes]):
        self.noise = NoiseConnection.from_name(name, noise_backend)

    def name_to_bytes(name: bytes) -> bytes:
      unpacked = name.decode().split('_')
      p  = unpacked[1]
      dh = unpacked[2]
      c  = unpacked[3]
      h  = unpacked[4]
      
      try:
        p_code = byte(NSPattern[NSPattern.prefix() + p])
      except KeyError:
        raise NoiseSocketError('Pattern ' + p + ' is not supported')
      
      try:
        dh_code = byte(NSCurve[NSCurve.prefix() + dh])
      except KeyError:
        raise NoiseSocketError('DH curve ' + dh + ' is not supported')
      
      try:
        c_code = byte(NSCipher[NSCipher.prefix() + c])
      except KeyError:
        raise NoiseSocketError('Cipher ' + c + ' is not supported')
      
      try:
        h_code = byte(NSHash[NSHash.prefix() + h])
      except KeyError:
        raise NoiseSocketError('Hash ' + h + ' is not supported')
      
      neg = b''.join([NoiseSocketConnection.version, dh_code, c_code, h_code, p_code])
      return neg

    def neg_to_name(neg: bytes) -> bytes:
      dh_code = neg[2]
      c_code  = neg[3]
      h_code  = neg[4]
      p_code  = neg[5]
      
      try:
        p = NSPattern(p_code).name[len(NSPattern.prefix()):].encode('utf-8')
      except ValueError:
        raise NoiseSocketError('Pattern with code ' + str(p_code) + ' is not supported')
      
      try:
        dh = NSCurve(dh_code).name[len(NSCurve.prefix()):].encode('utf-8')
      except ValueError:
        raise NoiseSocketError('DH curve with code ' + str(dh_code) + ' is not supported')
      
      try:
        c = NSCipher(c_code).name[len(NSCipher.prefix()):].encode('utf-8')
      except ValueError:
        raise NoiseSocketError('Cipher with code ' + str(c_code) + ' is not supported')
      
      try:
        h = NSHash(h_code).name[len(NSHash.prefix()):].encode('utf-8')
      except ValueError:
        raise NoiseSocketError('Hash with code ' + str(h_code) + ' is not supported')
      
      name = b'_'.join([b'Noise', p, dh, c, h])
      return name

    def set_psks(self, psk: Union[bytes, str] = None, psks: List[Union[str, bytes]] = None):
        self.noise.set_psks(psk, psks)

    def set_prologue(self, prologue: Union[bytes, str]):
        self.noise.set_prologue(prologue)

    def set_as_initiator(self):
        self.noise.set_as_initiator()

    def set_as_responder(self):
        self.noise.set_as_responder()

    def set_keypair_from_private_bytes(self, keypair: Keypair, private_bytes: bytes):
        self.noise.set_keypair_from_private_bytes(keypair, private_bytes)

    def set_keypair_from_public_bytes(self, keypair: Keypair, private_bytes: bytes):
        self.noise.set_keypair_from_public_bytes(keypair, private_bytes)

    def set_keypair_from_private_path(self, keypair: Keypair, path: str):
        self.noise.set_keypair_from_private_path(keypair, path)

    def set_keypair_from_public_path(self, keypair: Keypair, path: str):
        self.noise.set_keypair_from_public_path(keypair, path)

    def start_handshake(self):
        self.noise.start_handshake()

    def handshake_finished(self):
        if self.noise == None:
          return False
        return self.noise.handshake_finished

    # make NoiseSocket message, including Noise message
    def send_handshake_msg( self
                          , nt: NSNegotiationType
                          , payload: bytes=b''
                          , padding: bytes=b''
                          ):
        if   nt == NSNegotiationType.NOISE_SOCKET_PROTOCOL_PROPOSAL \
        or   nt == NSNegotiationType.NOISE_SOCKET_ACCEPTANCE:
          if self.noise == None:
            raise NoiseSocketError('No raised noise connection to write noise_message')
          neg = self.write_negotiation(nt)
          neglen = len(neg).to_bytes(2, byteorder='big')
          msg = self.write_noise_message(payload, padding)
          msglen = len(msg).to_bytes(2, byteorder='big')
        elif nt == NSNegotiationType.NOISE_SOCKET_REJECTION:
          neg = self.write_negotiation(nt, message == payload)
          neglen = len(neg).to_bytes(2, byteorder='big')
          msg = b''
          msglen = len(msg).to_bytes(2, byteorder='big')
        elif nt == NSNegotiationType.NOISE_SOCKET_REINITIALIZATION_REQUEST:
          if self.noise == None:
            raise NoiseSocketError('No raised noise connection to start negotiation')
          neg = self.write_negotiation(nt)
          neglen = len(neg).to_bytes(2, byteorder='big')
          msg = b''
          msglen = len(msg).to_bytes(2, byteorder='big')
        
        self.socket.sendall(neglen + neg)
        self.socket.sendall(msglen + msg)

    def write_negotiation( self
                         , nt: NSNegotiationType
                         , message: bytes=b''
                         ) -> bytearray:
        if   nt == NSNegotiationType.NOISE_SOCKET_PROTOCOL_PROPOSAL \
        or   nt == NSNegotiationType.NOISE_SOCKET_REINITIALIZATION_REQUEST:
          return NoiseSocketConnection.name_to_bytes(self.noise.protocol_name)
        elif nt == NSNegotiationType.NOISE_SOCKET_REJECTION:
          return message
        elif nt == NSNegotiationType.NOISE_SOCKET_ACCEPTANCE:
          return b''

    def write_noise_message(self, payload: bytes=b'', padding: bytes=b'') -> bytearray:
        padlen = len(payload).to_bytes(2, byteorder='big')
        return self.noise.write_message(padlen + payload + padding)

    #(NOISE_SOCKET_PROTOCOL_PROPOSAL, protocol_name, msgdata)
    #(NOISE_SOCKET_EXPLICIT_REJECTION, error_message, msgdata(empty))
    #(NOISE_SOCKET_ACCEPTANCE, negdata(empty), msgdata)
    #(NOISE_SOCKET_REINITIALIZATION_REQUEST, protocol_name, msgdata(empty))
    def recv_handshake_msg( self ) -> (NSNegotiationType, bytearray, bytearray):
        negdata = self.socket.recv(2)
        while len(negdata) != 2:
          negdata = self.socket.recv(2)
        neglen = int.from_bytes(negdata, byteorder='big')
        negdata = self.socket.recv(neglen)
        
        msgdata = self.socket.recv(2)
        while len(msgdata) != 2:
          msgdata = self.socket.recv(2)
        msglen = int.from_bytes(msgdata, byteorder='big')
        msgdata = self.socket.recv(msglen)
        
        if neglen == 0:
          # Acceptance: we can continue handshake
          # Return noise message
          nt = NSNegotiationType.NOISE_SOCKET_ACCEPTANCE
          return (nt, b'', msgdata)
        if msglen == 0:
          # Rejection or reinitialization request:
          # If negdata contains protocol proposal, it is a reinitialization
          # request. Otherwise it is rejection.
          # Return error message in case of explicit rejection
          # Return protocol name in case of reinitialization request
          nt, data = self.read_negotiation(negdata)
          if nt == NSNegotiationType.NOISE_SOCKET_PROTOCOL_PROPOSAL:
            nt = NSNegotiationType.NOISE_SOCKET_REINITIALIZATION_REQUEST
          return (nt, data, b'')
        else:
          # Protocol proposal:
          nt, protocol_name = self.read_negotiation(negdata)
          return (nt, protocol_name, msgdata)

    # Return type and protocol name or error message
    def read_negotiation(self, data: bytes) -> (NSNegotiationType, bytearray):
        if  len(data) == 6 \
        and data[0:2] == NoiseSocketConnection.version:
          # Protocol proposal or reinitialization request.
          # We cannot understand it on this level, so we reduce it
          # to protocol proposal
          return ( NSNegotiationType.NOISE_SOCKET_PROTOCOL_PROPOSAL
                 , NoiseSocketConnection.neg_to_name(data)
                 )
        else:
          return ( NSNegotiationType.NOISE_SOCKET_REJECTION
                 , data
                 )

    def read_noise_message(self, data: bytes) -> bytearray:
        buffer = self.noise.read_message(data)
        padlen = int.from_bytes(buffer[0:2], byteorder='big')
        return buffer[2:2+padlen]

    def send_transport_msg(self, data: bytes, padding: bytes=b''):
        padlen = len(data).to_bytes(2, byteorder='big')
        plaintext = padlen + data + padding
        ciphertext = self.noise.encrypt(plaintext)
        
        msglen = len(ciphertext).to_bytes(2, byteorder='big')
        msg = msglen + ciphertext
        self.socket.sendall(msg)

    def recv_transport_msg(self) -> bytearray:
        data = self.socket.recv(2)
        while len(data) != 2:
          data = self.socket.recv(2)
        dlen = int.from_bytes(data, byteorder='big')
        data = self.socket.recv(dlen)
        
        plaintext = self.noise.decrypt(data)
        padlen = int.from_bytes(plaintext[0:2], byteorder='big')
        return plaintext[2:2+padlen]

    def get_handshake_hash(self) -> bytes:
        return self.noise.get_handshake_hash()

    def rekey_inbound_cipher(self):
        self.noise.rekey_inbound_cipher()

    def rekey_outbound_cipher(self):
        self.noise.rekey_outbound_cipher()
