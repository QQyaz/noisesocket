import socket

from noise.connection import NoiseConnection, Keypair
from noisesocket import NoiseSocketConnection, NSNegotiationType

conn = socket.socket()
conn.connect(('localhost', 2000))

ns = NoiseSocketConnection(conn)
ns.make_from_name(b'Noise_XX_25519_AESCCM_SHA256')
print('Chosen protocol: ' + ns.noise.protocol_name.decode("utf-8"))

ns.set_as_initiator()

#ns.set_psks(bytes([88]*32))
#ns.set_keypair_from_private_bytes(Keypair.EPHEMERAL,     bytes([7]*32))
ns.set_keypair_from_private_bytes(Keypair.STATIC,        bytes([42]*32))

ns.start_handshake()

text = b'a'
ns.send_handshake_msg(NSNegotiationType.NOISE_SOCKET_PROTOCOL_PROPOSAL, text)
print("Sent:\n" + text.decode("utf-8") + "\n")

nt, negdata, msgdata = ns.recv_handshake_msg()
if nt != NSNegotiationType.NOISE_SOCKET_ACCEPTANCE:
  raise Exception('nscol is not accepted')
text = ns.read_noise_message(msgdata)
print("Received:\n" + text.decode("utf-8") + "\n")

text = b'c'
ns.send_handshake_msg(NSNegotiationType.NOISE_SOCKET_ACCEPTANCE, text)
print("Sent:\n" + text.decode("utf-8") + "\n")

# As of now, the handshake should be finished (as we are using NN pattern). 
# Any further calls to write_message or read_message would raise NoiseHandshakeError exception.
# We can use encrypt/decrypt methods of NoiseConnection now for encryption and decryption of messages.
text = b'Hello, world!'
ns.send_transport_msg(text)
print("Sent:\n" + text.decode("utf-8") + "\n")

text = ns.recv_transport_msg()
print("Received:\n" + text.decode("utf-8") + "\n")
