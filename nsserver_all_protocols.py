import socket
from threading import Thread
#from SocketServer import ThreadingMixIn 
from itertools import cycle

from noise.connection import NoiseConnection, Keypair
from noisesocket import NoiseSocketConnection, NSNegotiationType

acceptable = [ b'Noise_NN_25519_ChaChaPoly_SHA256'
             , b'Noise_NN_25519_AESGCM_SHA256'
             , b'Noise_NN_25519_AESCCM_SHA256'
             , b'Noise_NNpsk0_25519_ChaChaPoly_SHA256'
             , b'Noise_NNpsk0_25519_AESGCM_SHA256'
             , b'Noise_NNpsk0_25519_AESCCM_SHA256'
             , b'Noise_XX_25519_ChaChaPoly_SHA256'
             , b'Noise_XX_25519_AESGCM_SHA256'
             , b'Noise_XX_25519_AESCCM_SHA256'
             ]

class ClientThread(Thread): 
 
    def __init__(self, ip, port): 
        Thread.__init__(self) 
        self.ip = ip 
        self.port = port
        print('[+] New server socket thread started for ' + ip + ":" + str(port))
 
    def run(self): 
        while True :
            ns = NoiseSocketConnection(conn)
            
            # We accept anything and continue handshake, until its done
            for action in cycle(['receive', 'send']):
                if ns.handshake_finished():
                    br_a = action
                    break
                elif action == 'send':
                    text = b'Hey, Alice!'
                    ns.send_handshake_msg(NSNegotiationType.NOISE_SOCKET_ACCEPTANCE, text)
                    print('Sent:\n' + text.decode('utf-8') + '\n')
                elif action == 'receive':
                    nt, data, msg = ns.recv_handshake_msg()
                    if   nt == NSNegotiationType.NOISE_SOCKET_PROTOCOL_PROPOSAL:
                      print('Proposed protocol:\n' + data.decode('utf-8') + '\n')
                      
                      ns.make_from_name(data)
                      ns.set_as_responder()
                      init_primitives(ns)
                      ns.start_handshake()
                      
                      text = ns.read_noise_message(msg)
                      print('Received:\n' + text.decode('utf-8') + '\n')
                    elif nt == NSNegotiationType.NOISE_SOCKET_REJECTION:
                      print('Other side sent reject with error message:\n' + data.decode('utf-8') + '\n')
                    elif nt == NSNegotiationType.NOISE_SOCKET_ACCEPTANCE:
                      text = ns.read_noise_message(msg)
                      print('Received:\n' + text.decode('utf-8') + '\n')
                    elif nt == NSNegotiationType.NOISE_SOCKET_REINITIALIZATION_REQUEST:
                      print('Proposed protocol:\n' + data.decode('utf-8') + '\n')
                      
                      ns.make_from_name(data)
                      ns.set_as_initiator()
                      init_primitives(ns)
                      ns.start_handshake()
            
            print('Handshake done!\n')
            
            if br_a == 'receive':
              text = ns.recv_transport_msg()
              ns.send_transport_msg(text)
              print('Retranslated:\n' + text.decode('utf-8') + '\n')
            else:
              text = b'Hello, world!'
              ns.send_transport_msg(text)
              print('Translated:\n' + text.decode('utf-8') + '\n')
              text = ns.recv_transport_msg()
              ns.send_transport_msg(text)
              print('Retranslated:\n' + text.decode('utf-8') + '\n')

def hexBS(bts):
  return [hex(elem) for elem in bts]

def init_primitives(ns):
  ns.set_psks(bytes([88]*32))
  ns.set_keypair_from_private_bytes(Keypair.EPHEMERAL,     bytes([9]*32))
  ns.set_keypair_from_private_bytes(Keypair.STATIC,        bytes([27]*32))
  ns.set_keypair_from_private_bytes(Keypair.REMOTE_STATIC, bytes([42]*32))

if __name__ == '__main__':
  BUFFER_SIZE = 20  # Usually 1024, but we need quick response 
  
  tcpServer = socket.socket(socket.AF_INET, socket.SOCK_STREAM) 
  tcpServer.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1) 
  tcpServer.bind(('localhost', 2000)) 
  threads = []
   
  while True: 
      tcpServer.listen(4) 
      print('Multithreaded Python server : Waiting for connections from TCP clients...')
      (conn, (ip,port)) = tcpServer.accept()
      print('Accepted connection from', (ip, port))
      newthread = ClientThread(ip, port) 
      newthread.start() 
      threads.append(newthread) 
   
  for t in threads: 
      t.join()
